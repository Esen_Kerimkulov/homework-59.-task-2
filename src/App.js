import React, { Component } from 'react';
import Joke from './components/Joke/Joke';

class App extends Component {
  render() {
    return <Joke />;
  }
}

export default App;