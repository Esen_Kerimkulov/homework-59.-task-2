import React, { Component } from 'react';
import JokePost from "../../components/JokePost/JokePost";

class Joke extends Component {

    state = {
        joke: '',
    };

    componentDidMount() {
        fetch('https://api.chucknorris.io/jokes/random').then(response => {
            if (response.ok) {
                return response.json()
            }

            throw new Error ('Something wrong with network request')
        }).then(success => {
            this.setState({joke: success.value})
        })

    }

    render() {
        return (
            <div className="App">
                <JokePost joke={this.state.joke} />
                <img src="https://webiconspng.com/wp-content/uploads/2017/09/Chuck-Norris-PNG-Image-68588.png"/>
            </div>
        );
    }
}

export default Joke;