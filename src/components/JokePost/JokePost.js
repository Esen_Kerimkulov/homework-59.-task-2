import React from 'react';

const JokePost = (props) => {
    return (
        <div className="joke">
            <h1>{props.joke}</h1>
        </div>
    );
};

export default JokePost;